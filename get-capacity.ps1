function Get-Capacity ($ClusterName){
	$mygeneralreport = @()
	
	$ViewCluster = get-cluster $CLusterName | get-view
	$ObjVM = get-vm -location $ClusterName
	$NrOfHosts = $ViewCluster.Summary.NumHosts
	
	$OnVM = ($ObjVM | where {$_.PowerState -eq "PoweredOn"} | Measure-Object).Count
	$OnallocatedCPU = ($ObjVM | where{$_.PowerState -eq "PoweredOn"} | measure -InputObject {$_.NumCpu} -Sum).Sum
	$OnallocatedRAM = [Math]::Round(($ObjVM | where{$_.PowerState -eq "PoweredOn"} | measure -InputObject {$_.MemoryMB} -Sum).Sum/1024, 2)
	$OffVM = ($ObjVM | where {$_.PowerState -eq "PoweredOff"} | Measure-Object).Count
	$OffalocatedCPU = ($ObjVM | where{$_.PowerState -eq "PoweredOff"} | measure -InputObject {$_.NumCpu} -Sum).Sum
	$OffallocatedRAM = [Math]::Round(($ObjVM | where{$_.PowerState -eq "PoweredOff"} | measure -InputObject {$_.MemoryMB} -Sum).Sum/1024, 2)
	
	$TotalClusterRam = [Math]::Round($ViewCluster.Summary.TotalMemory/1Gb,2)
	$TotalCoresCpu = $ViewCluster.Summary.NumCpuCores
	$TotalLogicalCpu = $ViewCluster.Summary.NumCpuThreads
	$RatiovCpupCpu = [Math]::Round($OnallocatedCPU/$TotalCoresCpu, 2)
	
	$mygeneralreport += new-object psobject -property @{
				Cluster		 = $ClusterName
				CpuCores 	 = $TotalCoresCpu
				CpuLogical 	 = $TotalLogicalCpu 
				AllocatedCPU = $OnallocatedCPU
				VtoPCPU		 = $RatiovCpupCpu
				OffAllocatedCPU = $OffalocatedCPU
				TotalRam	 = $TotalClusterRam
				AllocatedRAM = $OnallocatedRAM
				OffallocatedRAM = $OffallocatedRAM
			}
	
	$mygeneralreport | select Cluster,CpuCores,CpuLogical,AllocatedCPU,VtoPCPU,TotalRam,AllocatedRAM,OffAllocatedCPU,OffallocatedRAM
	
	$myhostreport = @()
	foreach ($vmhost in get-cluster $CLusterName | get-vmhost){
		$ObjVM = get-vmhost $vmhost | get-vm
		$OnVM = ($ObjVM | where {$_.PowerState -eq "PoweredOn"} | Measure-Object).Count
		
		$OnallocatedCPU = ($ObjVM | where{$_.PowerState -eq "PoweredOn"} | measure -InputObject {$_.NumCpu} -Sum).Sum
		$OnallocatedRAM = [Math]::Round(($ObjVM | where{$_.PowerState -eq "PoweredOn"} | measure -InputObject {$_.MemoryMB} -Sum).Sum/1024,2)
		$TotalHostRam = [Math]::Round($vmhost.MemoryTotalGB, 2)
		[int]$TotalHostpCpu = $vmhost.numCpu
		
		$TotalHostLCpu = [Math]::Round($vmhost.numCpu * 2)
		$RatiovCpupCpu = [Math]::Round($OnallocatedCPU/$TotalHostpCpu, 2)
		
		$myhostreport += new-object psobject -property @{
			Host		 = $vmhost.name
			CpuCores 	 = $TotalHostpCpu
			CpuLogical 	 = $TotalHostLCpu 
			AllocatedCPU = $OnallocatedCPU
			VtoPCPU		 = $RatiovCpupCpu
			TotalRam	 = $TotalHostRam
			AllocatedRAM = $OnallocatedRAM
		}
		
	}
	$myhostreport | select host,cpucores,cpulogical,allocatedcpu,VtoPCPU,TotalRam,AllocatedRAM | Out-GridView
	
	write-host "Capacity per vm to host rules"
	foreach ($vmhostrule in get-drsrule -cluster $ClusterName -type vmhostaffinity){
		$vmvcpuallocated = 0
		$vmvramallocated = 0
		
		foreach ($vminrule in $vmhostrule.vmids){
			$vm = get-vm -id $vminrule
			$vmvcpuallocated = $vmvcpuallocated  + [int]$vm.numcpu
			$vmvramallocated = $vmvramallocated + [int]$vm.memorymb
		}
		
		$TotalAmmountofVMs  = $vmhostrule.vmids.count
		$TotalAllocatedvCpu = $vmvcpuallocated 
		$TotalAllocatedvRam = [Math]::Round($vmvramallocated/1024,2)
		
		$esxipcpu = 0
		$esxipram = 0
		
		foreach ($hostinrule in $vmhostrule.AffineHostIds){
			$esxi = get-vmhost -id $hostinrule
			$esxipcpu = $esxipcpu + [int]$esxi.numCpu
			$esxipram = $esxipram + [int]$esxi.MemoryTotalMB
		}
		
		$TotalAmmountofEsxis  = $vmhostrule.AffineHostIds.count
		$TotalPcpu = $esxipcpu
		$TotalPram = [Math]::Round($esxipram/1024,2)
		
		if ($vmhostrule.mandatory){
			$vmhostruletype = "Must"
		} else {
			$vmhostruletype = "Should"
		}
		
		$VtoPcpu = [Math]::Round($TotalAllocatedvCpu/$TotalPcpu, 2)
		
		
		write-host "[" $vmhostrule.name "] Type " $vmhostruletype
		write-host "[" $vmhostrule.name "] vCpu Allocated " $TotalAllocatedvCpu
		write-host "[" $vmhostrule.name "] pCpu Availabled " $TotalPcpu 
		write-host "[" $vmhostrule.name "] vCpu to pCpu ratio " $VtoPcpu
		write-host "[" $vmhostrule.name "] vRam Allocated " $TotalAllocatedvRam
		write-host "[" $vmhostrule.name "] pRam Available " $TotalPram
		write-host "[" $vmhostrule.name "] Ammount of VM's in rule " $TotalAmmountofVMs
		write-host "[" $vmhostrule.name "] Ammount of ESXi's in rule " $TotalAmmountofEsxis
		
	}
}


get-capacity -clustername "ClusterName"